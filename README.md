<p align="center">
    <h1 align="center">Blade</h1>
</p>

[Latest Stable Version](https://packagist.org/packages/e2es/blade)

The standalone version of [Laravel's Blade templating engine](https://laravel.com/docs/9.x/blade) for use outside of Laravel.

This package is based on [leafsphp/blade](https://github.com/leafsphp/blade).

## Installation

Install using composer:

```bash
composer require e2es/blade
```

## Usage

Create a Blade instance by passing it the folder(s) where your view files are located, and a cache folder. Render a template by calling the `make` method. More information about the Blade templating engine can be found on https://laravel.com/docs/9.x/blade.

```php
use EndToEnd\Blade;

$blade = new Blade('app/views', 'app/views/cache');

```

You can also initialise it globally and point to template directories later.

```php
$blade = new Blade;

// somewhere, maybe in a different file
$blade->configure("app/views", "app/views/cache");
```

```php
echo $blade->make('index', ['name' => 'Michael Darko'])->render();
```

Alternatively you can use the shorthand method `render`:

```php
echo $blade->render('index', ['name' => 'Michael Darko']);
```

We can have this as our template `index.blade.php`

```php
<!Doctype html>
<html>
    <head>
        <title>{{ $name }}</title>
    </head>
    <body>
        <div class="container">{{ $name }}</div>
    </body>
</html>
```

You can also extend Blade using the `directive()` function:

```php
$blade->directive('datetime', function ($expression) {
    return "<?php echo with({$expression})->format('F d, Y g:i a'); ?>";
});
```

Which allows you to use the following in your blade template:

```
Current date: @datetime($date)
```

The Blade instances passes all methods to the internal view factory. So methods such as `exists`, `file`, `share`, `composer` and `creator` are available as well. Check out the [original documentation](https://laravel.com/docs/9.x/views) for more information.
