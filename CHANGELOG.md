# Changelog

## E2ES Blade v1.5.0 - 11th February, 2022

### Added

- Support for Laravel/Illuminate 9.0

### Removed

- Removed support for PHP < 8.0.2

## Leaf Blade v1.2.4 - 20th January, 2021

### Added

- Added support for illuminate 8 packages

### Changed

- Made directory selection optional on Blade init

### Removed

- Nothing was removed

## Leaf Blade v1.2.3 - 7th March, 2020

### Added

- Added Configure Method

### Changed

- Made directory selection optional on Blade init

### Removed

- Nothing was removed
